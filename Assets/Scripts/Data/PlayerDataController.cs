﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Bigfoot;
using System;

[System.Serializable]
public class PlayerData
{
	public int Coins;
	public int XP;
	public int Level;
	public List<Item> Items;
}

public class PlayerDataController : Singleton<PlayerDataController> {

	public PlayerData Player;
	public PlayerData StartingValues;

	void OnEnable()
	{
		StoreEvents.OnItemBought += ItemBought;
	}

	void OnDisable()
	{
		StoreEvents.OnItemBought -= ItemBought;
	}

	private void ItemBought(Item i)
	{
		if (Player.Coins >= i.Price)
		{
			Player.Coins -= i.Price;
			Player.Items.Add(i);
			SaveData();
		}
	}

	protected override void Awake()
	{
		base.Awake();
		//Make sure the player data is ready as soon as we start the game
		LoadData();
	}

	void LoadData()
	{
		Player = Serializer.LoadSerializableObjectFromFile<PlayerData>(Application.persistentDataPath + "/data.dat");
		if(Player == null)
		{
			CreateData();
		}
	}

	void SaveData()
	{
		Serializer.SaveSerializableObjectToFile<PlayerData>(Player, Application.persistentDataPath + "/data.dat");
	}

	void CreateData()
	{
		Player = StartingValues;
		SaveData();
	}

#if UNITY_EDITOR
	[ContextMenu("Delete User Data")]
	void DeleteData()
	{
		UnityEditor.FileUtil.DeleteFileOrDirectory(Application.persistentDataPath + "/data.dat");
	}
#endif
}
