﻿using System.Collections;

public enum ItemType
{
	SHIP1,
	SHIP2,
	SHIP3
}

[System.Serializable]
public class Item {

	public ItemType Type;

	public int Price;
	public int Amount;

	/*int _price;
	int _amount;

	public virtual int GetPrice()
	{
		return _price;
	}

	public virtual int GetAmount()
	{
		return _amount;
	}*/
}