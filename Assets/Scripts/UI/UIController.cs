﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

	public GameObject Canvas;

	public GameObject DialogPrefab;

	void OnEnable()
	{
		GameEvents.OnPlayerDied += GameOver;
	}

	void OnDisable()
	{
		GameEvents.OnPlayerDied -= GameOver;
	}

	void GameOver()
	{
		GameObject dialog = GameObject.Instantiate (DialogPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		dialog.transform.SetParent(Canvas.transform,false);
		//dialog.transform.localScale = Vector3.one;

		dialog.GetComponent<Dialog> ().Config ("Game Over!", "Do you want to replay?", Replay, GoToMenu);
	}

	void Replay()
	{
		SceneController.Instance.ChangeScene ("Game");
	}

	void GoToMenu()
	{
		SceneController.Instance.ChangeScene ("Start");
	}


}
