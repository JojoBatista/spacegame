﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Page : MonoBehaviour 
{
	public Page parentPage;
	public List<GameObject> widgets;
	public List<CanvasGroup> canvasg;
	// Use this for initialization

	void OnEnable()
	{
		EnableWidgets (true);
	}

	void OnDisable ()
	{
		EnableWidgets (false);
	}

	public void Leave()
	{
		gameObject.SetActive (false);
		parentPage.gameObject.SetActive (true);
	}

	public void EnableWidgets(bool enable)
	{
		Debug.Log ("Enable" + enable);
		if (enable) 
		{
			FadeIn (0.7f);
		} 
		else 
		{
			foreach (CanvasGroup cg in canvasg) 
			{
				cg.alpha = 0;
				cg.interactable = true;
			}
		}

		foreach (GameObject go in widgets) 
		{
			go.SetActive (enable);
		}
	}

	public void FadeIn(float sec)
	{
		StartCoroutine (WaitSeconds (sec));
	}

	IEnumerator WaitSeconds(float sec)
	{
		Debug.Log ("WaitSeconds" + sec);
		yield return new WaitForSeconds (sec);
		while (canvasg [0].alpha < 1) 
		{
			foreach (CanvasGroup cg in canvasg) 
			{
				if(cg.alpha == 0)
					cg.interactable = true;
				cg.alpha += Time.deltaTime / 1f;
			}
			yield return null;
		}

	}


}
