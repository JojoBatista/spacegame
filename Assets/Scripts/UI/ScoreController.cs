﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using UnityEngine;

public class ScoreController : MonoBehaviour {

	public Text ScoreText;

	private int currentScore = 0;

	void AddScore(int score)
	{
		currentScore += score;
		ScoreText.text = currentScore.ToString ();
	}

	void OnEnable()
	{
		GameEvents.OnEnemyDied += EnemyDied;
	}

	void OnDisable()
	{
		GameEvents.OnEnemyDied -= EnemyDied;
	}

	void EnemyDied(int score)
	{
		AddScore (score);
	}
}