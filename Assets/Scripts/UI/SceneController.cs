﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using UnityEngine;

public class SceneController : Singleton<SceneController> {
	
	public void ChangeScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}

}
