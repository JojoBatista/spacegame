﻿using UnityEngine;
using System.Collections;

public class LoadingController : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		Debug.Log ("Estoy cargando cosas....");
		Invoke ("ChangeScene", 1);
	}

	void ChangeScene () 
	{
		SceneController.Instance.ChangeScene ("Start");
	}
}
