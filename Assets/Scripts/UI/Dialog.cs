﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Dialog : MonoBehaviour {

	public Text Title;

	public Text Description;

	Action _onYes;

	Action _onNo;

	public void Config(string title, string description, Action onYes, Action onNo)
	{
		Title.text = title;
		Description.text = description;
		_onYes = onYes;
		_onNo = onNo;
	}

	public void YesPressed()
	{
		if (_onYes != null)
			_onYes ();
	}

	public void NoPressed()
	{
		if (_onNo != null)
			_onNo();
	}
}
