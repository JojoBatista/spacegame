﻿using UnityEngine;
using System.Collections;

public class ChangeSceneButton : MonoBehaviour {

	public void Change(string name)
	{
		SceneController.Instance.ChangeScene (name);
	}
}
