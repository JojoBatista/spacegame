using UnityEngine;
using System.Collections;

public class MainPage : Page 
{
	public GameObject soundPage;
	public GameObject instructionsPage;
	public GameObject Store;

	void Start()
	{
		EnableWidgets (true);
	}

	public void OnSoundPage()
	{
		gameObject.SetActive (false);
		soundPage.SetActive (true);
	}

	public void OnInstructionPage()
	{
		Debug.Log ("OnInstructionPage");
		gameObject.SetActive (false);
		instructionsPage.SetActive (true);
	}

	public void OnShop()
	{
		Store.SetActive(true);
		gameObject.SetActive(false);
	}

	public void OnStartGame()
	{

	}

	public void OnEndGame()
	{

	}
}
