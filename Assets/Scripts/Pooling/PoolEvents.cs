﻿using System;
using UnityEngine;

public static class PoolEvents  {

	public static Action<GameObject> OnPoolObject;

	public static void PoolObject(GameObject go)
	{
		if (OnPoolObject != null) {
			OnPoolObject (go);
		}
	}
}
