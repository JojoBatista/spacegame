﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float Speed = 20;
	public int Score;
    public string ExplotionAnimation = "EnemyExplotion";
    Animator _anim;

	// Use this for initialization
	void Start () {
	    //TODO: Mover enemigo, de 3 formas diferentes, usando animaciones
        _anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Time.deltaTime * Speed * -Vector3.right;
	}

    void OnTriggerEnter2D(Collider2D col)                                                               
    {
		if (!col.CompareTag("Enemy") && !col.CompareTag("PoolEnd"))
        {
			GameEvents.EnemyDied(Score);
            _anim.Play(ExplotionAnimation);            
            Destroy(col.gameObject);
        }
    }

    public void DestroyEnemy()
    {
        Destroy(gameObject);
    }
}
