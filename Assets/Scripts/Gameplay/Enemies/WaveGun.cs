﻿using UnityEngine;
using System.Collections;
using System;

public class WaveGun : MonoBehaviour {

	public GameObject Bullet;
	public float TimeBetweenWaves;

	public void StartWaves(int waves)
	{
		StartCoroutine(ShootWaves(waves));
	}

	IEnumerator ShootWaves(int waves)
	{
		int amountShoted = 0;
		while(amountShoted < waves)
		{
			ShootSingleWave();
			amountShoted++;
			yield return new WaitForSeconds(TimeBetweenWaves);
		}
	}

	private void ShootSingleWave()
	{
		foreach(Transform t in transform)
		{
			var go = Instantiate(Bullet) as GameObject;
			go.transform.parent = t;
			go.transform.localPosition = Vector3.zero;
			go.transform.localEulerAngles = Vector3.zero;
		}
	}
}
