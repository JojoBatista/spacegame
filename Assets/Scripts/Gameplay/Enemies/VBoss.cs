﻿using UnityEngine;
using System.Collections;

public enum VBossState
{
	IDLE,
	PRE_SHOOTING_WAVE,
	SHOOTING_WAVE,
	PRE_SHOOTING_NORMAL,
	SHOOTING_NORMAL,
	PRE_SHOOT_BIG,
	SHOOTING_BIG
}

public class VBoss : MonoBehaviour {

	[Header("Boss Config")]
	public int HP = 100;
	int _currentHP;
	
	[Header("Gun Config")]
	public WaveGun Wavegun;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator RunPhase1()
	{
		//Si todavia tiene mas de la mitad de vida, corre esta fase
		while(_currentHP > HP / 2)
		{
			Wavegun.StartWaves(2);
			yield return new WaitForSeconds(2f);
			
		}
	}
}
