﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float Speed = 12;
    public GameObject BulletPrefab;
    public float FireRate = 0.3f;
    //Cuanto tiempo falta hasta poder dar el proximo tiro
    float _timeElapsed;

	void Start () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag ("Enemy")) {
			Die ();
		}
	}

	void Die()
	{
		Destroy (gameObject);
		GameEvents.PlayerDied ();
	}

    /// <summary>
    /// Se llama una vez por frame
    /// </summary>
	void Update () {
        Move();
        Shoot();
	}

    public void Move()
    {
        //Tomamos el input horizontal y lo guardamos en la variable h
        float h = Input.GetAxisRaw("Horizontal");
        //Tomamos el input vertical y lo guardamos en la variable v
        float v = Input.GetAxisRaw("Vertical");
        //Calculamos la proxima posicion del jugador
        Vector3 newPos = transform.position + new Vector3(h, v, 0) * Speed * Time.deltaTime;

        //Cambiamos la posicion del jugador 
        if(InBoundaries(newPos, Camera.main))
        {
            //TODO: Se me traba mientras estoy en un limite y me muevo validamente en el otro eje
            transform.position = newPos;
        }
    }

    bool InBoundaries(Vector3 pos, Camera cam)
    {
        Vector3 posInScreen = cam.WorldToScreenPoint(pos);
        if (posInScreen.x > 0 && posInScreen.y > 0 && posInScreen.x < cam.pixelRect.width && posInScreen.y < cam.pixelRect.height)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Dispara una bala
    /// </summary>
    public void Shoot()
    {
        //Si podemos disparar
        if (_timeElapsed <= 0)
        {
            //Si el usuario apreta el boton Fire1
            if (Input.GetButton("Fire1"))
            {
                //Reseteamos el valor que tenemos que esperar
                _timeElapsed = FireRate;
                //Instanciamos una bala en la escena, en la posicion del jugador
                Instantiate(BulletPrefab, transform.position, Quaternion.identity);
            }
        }
        else
        {
            //Sino, restamos tiempo al _timeElapsed
            _timeElapsed -= Time.deltaTime;
        }
    }
}
