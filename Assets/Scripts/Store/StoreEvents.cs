﻿using System;

public static class StoreEvents   {

	public static Action<Item> OnTryBuy;

	public static void TryBuy(Item i)
	{
		if (OnTryBuy != null)
			OnTryBuy(i);
	}

	public static Action<Item> OnItemBought;
	public static void ItemBought(Item i)
	{
		if (OnItemBought != null)
			OnItemBought(i);
	}

	public static Action<Item> OnEquipItem;
	public static void EquipItem(Item i)
	{
		if (OnEquipItem != null)
			OnEquipItem(i);
	}
}