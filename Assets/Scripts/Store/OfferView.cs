﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum OfferViewState
{
	LOCKED,
	UNLOCKED,//unlocked but not bought
	EQUIPPED,//bought but equipped
	UNEQUIPPED//bought but not equipped
}

public class OfferView : MonoBehaviour {

	public GameObject BuyPanel;
	public Text Price;
	public Image OfferImage;
	Item _item;
	OfferViewState _state;

	public void Config(Item item, OfferViewState state)
	{
		this._item = item;
		if (state == OfferViewState.UNLOCKED)
		{
			if (Price != null)
				Price.text = item.Price.ToString();
			if (OfferImage != null)
				OfferImage.sprite = Resources.Load<Sprite>(item.Type.ToString());
		}
		else
		{
			BuyPanel.SetActive(false);
		}
	}

	public void Buy()
	{
		if (_state == OfferViewState.UNLOCKED)
			StoreEvents.TryBuy(_item);
		else if (_state == OfferViewState.UNEQUIPPED)
			StoreEvents.EquipItem(_item);
	}
}
