﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StoreController : MonoBehaviour {

	public List<Item> Offers;
	public GameObject OfferPrefab;
	PlayerDataController _pdc;

	// Use this for initialization
	void Start () {
		_pdc = PlayerDataController.Instance;
		foreach(var o in Offers)
		{
			AddOffer(o);
		}
	}

	void AddOffer(Item i)
	{
		var go = Instantiate(OfferPrefab) as GameObject;
		go.transform.SetParent(transform);
		var offer = go.GetComponent<OfferView>();
		if(offer != null)
		{
			offer.Config(i, OfferViewState.UNLOCKED);
		}
	}
	
	void OnEnable()
	{
		StoreEvents.OnTryBuy += TryBuyItem;
	}

	void OnDisable()
	{
		StoreEvents.OnTryBuy -= TryBuyItem;
	}

	private void TryBuyItem(Item i)
	{
		if(_pdc.Player.Coins >= i.Price)
		{
			StoreEvents.ItemBought(i);
		}
		else
		{
			//Show dialog saying that you don't have enough coins
		}
	}
}
