﻿using System;

public static class GameEvents {

	public static Action<int> OnEnemyDied;

	public static void EnemyDied(int score)
	{
		if (OnEnemyDied != null) {
			OnEnemyDied (score);
		}
	}

	public static Action OnPlayerDied;

	public static void PlayerDied()
	{
		if (OnPlayerDied != null) {
			OnPlayerDied ();
		}
	}

}